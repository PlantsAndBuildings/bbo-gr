#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>

typedef std::vector< std::vector<int> > horT;
typedef std::vector< std::vector<int> > verT;
typedef std::pair<horT,verT> layerT;

using std::cin;
using std::cout;
using std::fstream;
using std::string;
using std::ios;

class Grid{
	public:

	Grid();
	Grid(fstream & fin);
	
	private:
		int xGrids; 
		int yGrids; 
		int numLayers;
		std::vector<int> hCap, vCap;
		std::vector<int> minWidth;
		std::vector<int> minSpacing;
		std::vector<int> viaSpacing;
		int llx, lly;
		int tWidth, tHeight;
		std::vector<layerT> layer; 
	public:
		void buildLayers(int numLayers, std::vector<int>& hCap, std::vector<int>& vCap);
		void printGrid();
		void printLayer(layerT & l);
		void assignCapacitiesToEdges();

};

Grid::Grid(fstream & fin){
	// Parsing grid information
	string tmpStr;
	fin>>tmpStr;	// "grid"
	fin>>xGrids>>yGrids>>numLayers;
	hCap.resize(numLayers);
	vCap.resize(numLayers);
	fin>>tmpStr>>tmpStr;	// "vertical" and "capacity"
//	cout<<"vCap:\n";
	for(int i=0; i<numLayers; i++){
		fin>>vCap[i];
//		cout<<vCap[i]<<" ";
	}
//	cout<<"\n";
	fin>>tmpStr>>tmpStr;	// "horizontal" and "capacity"
//	cout<<"hCap:\n";
	for(int i=0; i<numLayers; i++){
		fin>>hCap[i];
//		cout<<hCap[i]<<" ";
	}
//	cout<<"\n";
	minWidth.resize(numLayers);	// minimum wire width
	minSpacing.resize(numLayers);	// minimum wire spacing
	viaSpacing.resize(numLayers);	// minimum via spacing
//	cout<<"minWidth:\n";
	for(int i=0; i<numLayers; i++){
		fin>>minWidth[i];
//		cout<<minWidth[i]<<" ";
	}
//	cout<<"\n";
//	cout<<"minSpacing:\n";
	for(int i=0; i<numLayers; i++){
		fin>>minSpacing[i];
//		cout<<minSpacing[i]<<" ";
	}
//	cout<<"\n";
//	cout<<"viaSpacing:\n";
	for(int i=0; i<numLayers; i++){
		fin>>viaSpacing[i];
//		cout<<viaSpacing[i]<<" ";
	}
//	cout<<"\n";
	fin>>llx>>lly;
	fin>>tWidth>>tHeight;
	layer.resize(numLayers);
	for(int i=0; i<numLayers; i++){
		layer[i].first.resize(yGrids);
		for(int j=0; j<yGrids; j++){
			layer[i].first[j].resize(xGrids-1);
		}
		layer[i].second.resize(yGrids-1);
		for(int j=0; j<(yGrids-1); j++){
			layer[i].second[j].resize(xGrids);
		}
	}
	assignCapacitiesToEdges();
}

void Grid::buildLayers(int numLayers, std::vector<int>& hCap, std::vector<int> & vCap){
	for(int i=0; i<numLayers; i++){
	}
}

void Grid::printGrid(){
	for(int i=0; i<numLayers; i++){
		std::cout<<"Layer No. "<<i<<"\n";
		printLayer(layer[i]);
	}
}

void Grid::printLayer(layerT & l){
	std::cout<<"Horizontal: \n";
	for(int i=0; i < l.first.size(); i++){
		for(int j=0; j < l.first[i].size(); j++){
			std::cout<<l.first[i][j]<<" ";
		}
		std::cout<<"\n";
	}
	std::cout<<"Vertical: \n";
	for(int i=0; i < l.second.size(); i++){
		for(int j=0; j < l.second[i].size(); j++){
			std::cout<<l.second[i][j]<<" ";
		}
		std::cout<<"\n";
	}
}

void Grid::assignCapacitiesToEdges(){
	// For Each layer
	for(int layerNum=0; layerNum<numLayers; layerNum++){
		// First Assign capacities to horizontal edges
		for(int j=0; j<layer[layerNum].first.size(); j++){
			for(int k=0; k<layer[layerNum].first[j].size(); k++){
				layer[layerNum].first[j][k] = hCap[layerNum];
			}
		}
		// Then assign capacities to vertical edges
		for(int j=0; j<layer[layerNum].second.size(); j++){
			for(int k=0; k<layer[layerNum].second[j].size(); k++){
				layer[layerNum].second[j][k] = vCap[layerNum];
			}
		}
	}
}

class Net{
	
};


// Driver function to test.. remove later.
int main(int argc, char ** argv){
	
	
	int xGrids; 
	int yGrids; 
	int numLayers;
	std::vector<int> hCap, vCap;
	std::vector<int> minWidth;
	std::vector<int> minSpacing;
	std::vector<int> viaSpacing;
	int llx, lly;
	int tWidth, tHeight;
	int numNets;
	
	fstream fin, fout;
	fin.open(argv[1],ios::in);
	fout.open(argv[2],ios::out);
	
	Grid g(fin);

	g.printGrid();
	// Parsing information about nets
//	fin>>tmpStr>>tmpStr;	// "num" "net"
	fin.close();
	fout.close();
	return 0;
}
